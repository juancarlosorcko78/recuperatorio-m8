FROM node:16.19.0-alpine3.16
RUN npm install -g http-server
WORKDIR /client
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
EXPOSE 8081
CMD [ "http-server", "dist" ]