FROM python:3.9.16-alpine3.16
WORKDIR /server
COPY requirements.txt /server/requirements.txt
RUN cd /server
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . . 
CMD ["flask", "run"]